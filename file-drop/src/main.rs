#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;

#[get("/")]
fn index() -> &'static str {
  "Upload your text files by POSTing them to /upload."
}

#[get("/<file>")]
fn file_get(file: String) -> String {
  format!("File: {}", file)
}

fn main() {
  rocket::ignite().mount("/", routes![index, file_get]).launch();
}
